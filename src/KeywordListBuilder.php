<?php

namespace Drupal\alinks;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Keyword entities.
 *
 * @ingroup alinks
 */
class KeywordListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\alinks\Entity\Keyword $entity */
    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url(
      'entity.alink_keyword.edit_form', [
        'alink_keyword' => $entity->id(),
      ]
    ));
    return $row + parent::buildRow($entity);
  }

}
