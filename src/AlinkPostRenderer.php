<?php

namespace Drupal\alinks;

use Drupal\alinks\Entity\Keyword;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Wamania\Snowball\English;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides post renderer functionality.
 */
class AlinkPostRenderer implements TrustedCallbackInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected $content;

  /**
   * {@inheritdoc}
   */
  protected $keywords;

  /**
   * {@inheritdoc}
   */
  protected $existingLinks;

  /**
   * Stemmer.
   *
   * @var \Wamania\Snowball\Stem
   */
  protected $stemmer;

  /**
   * {@inheritdoc}
   */
  protected $stemmerCache = [];

  /**
   * {@inheritdoc}
   */
  protected $xpathSelector = "//text()[not(ancestor::a) and not(ancestor::script) and not(ancestor::*[@data-alink-ignore])]";

  /**
   * The replace method: first or all.
   *
   * @var string
   */
  protected mixed $replaceMethod;


  /**
   * AlinkPostRenderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\Markup|string $content
   *   The content of the current page.
   * @param array $context
   *   The current page context.
   * @param null $xpathSelector
   *   The selector rule for the html.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, $content, array $context = NULL, $xpathSelector = NULL) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->replaceMethod = \Drupal::config('alinks.settings')->get('replace_method') ?? 'replace_first';
    if (!empty($context['#entity_type']) && !empty($context['#' . $context['#entity_type']])) {
      $entity = $context['#' . $context['#entity_type']];
      $class = 'Wamania\Snowball\\' . $entity->language()->getName();
      if (class_exists($class)) {
        $this->stemmer = new $class();
      }
      else {
        $this->stemmer = new English();
      }
    }

    $this->content = $content;
    if ($xpathSelector) {
      $this->xpathSelector = $xpathSelector;
    }
  }

  /**
   * Load alinks keywords.
   *
   * @return \Drupal\alinks\Entity\Keyword[]
   *   Returns a list of all of the alinks keywords.
   */
  protected function getKeywords() {
    if ($this->keywords === NULL) {
      $ids = \Drupal::entityQuery('alink_keyword')
        ->condition('status', 1)
        ->accessCheck(FALSE)
        ->execute();
      $this->keywords = Keyword::loadMultiple($ids);
      $config = $this->configFactory->get('alinks.settings');
      $vocabularies = $config->get('vocabularies');

      if ($vocabularies) {
        $terms = \Drupal::entityQuery('taxonomy_term')
          ->condition('vid', $vocabularies, 'IN')
          ->accessCheck(FALSE)
          ->execute();

        $terms = Term::loadMultiple($terms);
        foreach ($terms as $term) {
          $this->keywords[] = Keyword::create([
            'name' => $term->label(),
            'link' => [
              'uri' => 'internal:/' . $term->toUrl()->getInternalPath(),
            ],
          ]);
        }
      }

      foreach ($this->keywords as &$keyword) {
        $keyword->stemmed_keyword = $this->stemmer->stem($keyword->getText());
      }
    }
    return $this->keywords;
  }

  /**
   * Set keywords and stemmed keywords.
   *
   * @param mixed $keywords
   *   A list of all of the keywords to set.
   */
  public function setKeywords($keywords) {
    $this->keywords = $keywords;
    foreach ($this->keywords as &$keyword) {
      $keyword->stemmed_keyword = $this->stemmer->stem($keyword->getText());
    }
  }

  /**
   * Load the content and replace the matched strings with automatic links.
   */
  public function replace() {
    $dom = Html::load($this->content);
    $xpath = new \DOMXPath($dom);

    $this->existingLinks = $this->extractExistingLinks($xpath);
    $this->keywords = array_filter($this->getKeywords(), function (Keyword $word) {
      return !isset($this->existingLinks[$word->getUrl()]);
    });

    foreach ($xpath->query($this->xpathSelector) as $node) {
      $text = $node->wholeText;
      $replace = FALSE;
      if (empty(trim($text))) {
        continue;
      }

	  $already_tested_keywords = [];
      foreach ($this->keywords as $word) {
        if (isset($already_tested_keywords[$word->getText() . $word->getUrl()])) {
          // Don't re-try keyword-replacement for keyword-url pairs that have
          // been already processed earlier. This is necessary because
          // $this->addExistingLink() removes keywords from $this->keywords but
          // foreach will keep working on the "old" array.
          // @see: https://stackoverflow.com/a/14854568/880188
          continue;
        }
        if ($this->replaceMethod == 'replace_all') {
          $text = $this->replaceAll($word, '<a href="' . $word->getUrl() . '">' . $word->getText() . '</a>', $text, $count);
        }
        else {
          $text = $this->replaceFirst($word, '<a href="' . $word->getUrl() . '">' . $word->getText() . '</a>', $text, $count);
        }

        // Remember that for the current keyword-url pair was tested
        // (replacement-function called).
        $already_tested_keywords[$word->getText() . $word->getUrl()] = TRUE;

        if ($count) {
          $replace = TRUE;
          $this->addExistingLink($word);
        }
      }
      if ($replace) {
        $this->replaceNodeContent($node, $text);
      }
    }

    return Html::serialize($dom);
  }

  /**
   * Process the node list to replace links.
   */
  protected function processDomNodeList($element) {
    foreach ($element as $item) {
      if ($item instanceof \DOMElement) {
        if ($item->hasChildNodes()) {
          foreach ($item->childNodes as $childNode) {
            if ($childNode instanceof \DOMText) {
              foreach ($this->getKeywords() as $word) {

                if ($this->replaceMethod == 'replace_all') {
                  $childNode->nodeValue = $this->replaceAll($word, '<a href="' . $word->getUrl() . '">' . $word->getText() . '</a>', $childNode->nodeValue);
                }
                else {
                  $childNode->nodeValue = $this->replaceFirst($word, '<a href="' . $word->getUrl() . '">' . $word->getText() . '</a>', $childNode->nodeValue);
                }
              }
            }
          }
        }
      }
    }

    return $element;
  }

  /**
   * Replace functionality.
   *
   * @param \Drupal\alinks\Entity\Keyword $search
   *   We extract the pattern to search for.
   * @param array|string $replace
   *   The string or an array with strings to replace.
   * @param array|string $subject
   *   The string or an array with strings to search and replace.
   * @param int|null $count
   *   This variable will be filled with the number of replacements done.
   *
   * @return string
   *   The updated subject after replaces.
   */
  protected function replaceAll(Keyword $search, $replace, $subject, &$count = 0) {
    $subject = str_replace($search->getText(), $replace, $subject, $count);
    if ($count == 0) {
      // @todo Try stemmer
    }
    return $subject;
  }

  /**
   * Uses regular expression to replace the first matched keyword in content.
   *
   * @param \Drupal\alinks\Entity\Keyword $search
   *   We extract the pattern to search for.
   * @param array|string $replace
   *   The string or an array with strings to replace.
   * @param array|string $subject
   *   The string or an array with strings to search and replace.
   * @param int|null $count
   *   This variable will be filled with the number of replacements done.
   *
   * @return string
   *   The updated subject after replaces.
   */
  protected function replaceFirst(Keyword $search, $replace, $subject, &$count = 0) {
    $search_escaped = preg_quote($search->getText(), '/');
    $subject = preg_replace('/\b' . $search_escaped . '\b/u', $replace, $subject, 1, $count);
    if ($count == 0) {

      // @todo Look at Search API Tokenizer & Highlighter
      $terms = str_replace(['.', ',', ';', '!', '?'], '', $subject);
      $terms = explode(' ', $terms);
      $terms = array_filter(array_map('trim', $terms));
      $terms = array_combine($terms, $terms);
      $terms = array_map(function ($term) {
        if (!isset($this->stemmerCache[$term])) {
          $this->stemmerCache[$term] = $this->stemmer->stem($term);
        }
        return $this->stemmerCache[$term];
      }, $terms);
      foreach ($terms as $original_term => $term) {
        if ($term === $search->stemmed_keyword) {
          $search_escaped = preg_quote($original_term, '/');
          $subject = preg_replace('/\b' . $search_escaped . '\b/u', '<a href="' . $search->getUrl() . '">' . $original_term . '</a>', $subject, 1, $count);
        }
      }
    }

    return $subject;
  }

  /**
   * Provides the post render functionality.
   *
   * @param \Drupal\Core\Render\Markup $content
   *   The content in the markup format.
   * @param array $context
   *   The current page context.
   *
   * @return string
   *   The replaced string
   */
  public static function postRender(Markup $content, $context, EntityTypeManagerInterface $entityTypeManager) {
    $selector = \Drupal::config('alinks.settings')->get('xpathSelector');
    $renderer = new static($entityTypeManager, \Drupal::configFactory(), $content, $context, $selector);

    return $renderer->replace();
  }

  public static function trustedCallbacks() {
      return ['postRender'];
  }
  
  /**
   * Normalize the URLs with front links and internal links.
   *
   * @param string $uri
   *   A url to be normalized.
   *
   * @return string
   *   The normalized URL.
   */
  protected function normalizeUri($uri) {

    // If we already have a scheme, we're fine.
    if (empty($uri) || !is_null(parse_url($uri, PHP_URL_SCHEME))) {

      return $uri;
    }

    // Remove the <front> component of the URL.
    if (strpos($uri, '<front>') === 0) {
      $uri = substr($uri, strlen('<front>'));
    }

    // Add the internal: scheme and ensure a leading slash.
    return 'internal:/' . ltrim($uri, '/');
  }

  /**
   * Extract all of the links in an xpath query.
   *
   * @param string $xpath
   *   An xpath match to parse for links.
   *
   * @return array
   *   Unique links from an xpath.
   */
  protected function extractExistingLinks($xpath) {

    // @todo Remove keywords with links which are already in the text
    $links = [];

    foreach ($xpath->query('//a') as $link) {
      try {
        $uri = $this->normalizeUri($link->getAttribute('href'));
        $links[] = Url::fromUri($uri)->toString();
      }
      catch (\Exception $exception) {
        // Do nothing.
      }
    }

    return array_flip(array_unique($links));
  }

  /**
   * Check to see if keywords on this object match the passed word.
   *
   * @param \Drupal\alinks\Entity\Keyword $word
   *   An individual keyword.
   */
  protected function addExistingLink(Keyword $word) {
    $this->existingLinks[$word->getUrl()] = TRUE;
    $this->keywords = array_filter($this->keywords, function ($keyword) use ($word) {
      if ($keyword->getText() == $word->getText()) {

        return FALSE;
      }
      if ($keyword->getUrl() == $word->getUrl()) {

        return FALSE;
      }

      return TRUE;
    });
  }

  /**
   * Replace the contents of a DOMNode.
   *
   * @param \DOMNode $node
   *   A DOMNode object.
   * @param string $content
   *   The text or HTML that will replace the contents of $node.
   */
  protected function replaceNodeContent(\DOMNode &$node, $content) {
    if (strlen($content)) {

      // Load the content into a new DOMDocument and retrieve the DOM nodes.
      $replacement_nodes = Html::load($content)->getElementsByTagName('body')
        ->item(0)
        ->childNodes;
    }
    else {
      $replacement_nodes = [$node->ownerDocument->createTextNode('')];
    }

    foreach ($replacement_nodes as $replacement_node) {

      // Import the replacement node from the new DOMDocument into the original
      // one, importing also the child nodes of the replacement node.
      $replacement_node = $node->ownerDocument->importNode($replacement_node, TRUE);
      $node->parentNode->insertBefore($replacement_node, $node);
    }
    $node->parentNode->removeChild($node);
  }

}
